package com.arifhermawan_10161017.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class PersistenceActivity extends AppCompatActivity {

    EditText textInput;
    Button textSave;
    TextView textOutput;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistence);

        textInput = findViewById(R.id.textInput);
        textSave = findViewById(R.id.textSave);
        textOutput = findViewById(R.id.textOutput);

        sharedPreferences = getSharedPreferences("datapersistance", MODE_PRIVATE);

        textSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(PersistenceActivity.this, "Data Saved!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getData() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("save_data", textInput.getText().toString() );
        editor.apply();
        textOutput.setText("Saved Data :" + sharedPreferences.getString("save_data", null));


    }
}